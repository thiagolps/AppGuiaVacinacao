package com.thiago.appguiavacinacao;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DetalheVacina extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe_vacina);

        Intent it = getIntent();
        if (it != null) {
            Bundle extras = it.getExtras();
            String vacina = extras.getString("vacina");
            String doenca = extras.getString("doenca");
            String idade = extras.getString("idade");
            String dose = extras.getString("dose");

            TextView Vacina = (TextView) findViewById(R.id.txtDetVacina);
            Vacina.setText(vacina);
            TextView Doenca = (TextView) findViewById(R.id.txtDetDoenca);
            Doenca.setText(doenca);
            TextView Idade = (TextView) findViewById(R.id.txtDetIdade);
            Idade.setText(idade);
            TextView Dose = (TextView) findViewById(R.id.txtDetDose);
            Dose.setText(dose);

        }

        Button btnVoltar = (Button) findViewById(R.id.btnVoltar);
        btnVoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


    }

}
