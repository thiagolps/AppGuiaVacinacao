package com.thiago.appguiavacinacao.adapter;

/**
 * Created by thiago on 01/05/17.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.thiago.appguiavacinacao.ListaVacinas;
import com.thiago.appguiavacinacao.R;
import com.thiago.appguiavacinacao.model.CalVacinas;

public class VacinaAdapter extends BaseAdapter {

    private Context ctx;
    private CalVacinas[] vacinas;

    public VacinaAdapter(Context ctx, CalVacinas[] vacinas) {
        this.ctx = ctx;
        this.vacinas = vacinas;
    }


    @Override
    public int getCount() {
        return (vacinas != null) ? vacinas.length : 0;
    }

    @Override
    public Object getItem(int i) {
        return vacinas[i];
    }

    @Override
    public long getItemId(int i) {
        return Long.parseLong(vacinas[i].getID());
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        // Criar a view
        ViewHolder holder = null;

        if (convertView == null) {
            convertView = LayoutInflater.from(ctx).inflate(R.layout.item_vacina, null);

            holder = new ViewHolder();
            holder.imgLogo = (ImageView) convertView.findViewById(R.id.imgLogo);
            holder.txtVacina = (TextView) convertView.findViewById(R.id.txtVacina);
            holder.txtIdade = (TextView) convertView.findViewById(R.id.txtIdade);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.imgLogo.setImageResource(R.mipmap.ic_launcher);
        holder.txtVacina.setText(vacinas[i].getVacina());
        holder.txtIdade.setText(vacinas[i].getIdade());

        return convertView;
    }

    static class ViewHolder {
        ImageView imgLogo;
        TextView txtVacina;
        TextView txtIdade;
    }
}
