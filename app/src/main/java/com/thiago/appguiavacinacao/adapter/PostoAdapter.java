package com.thiago.appguiavacinacao.adapter;

/**
 * Created by rbertoli on 03/05/17.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.thiago.appguiavacinacao.ListaVacinas;
import com.thiago.appguiavacinacao.R;
import com.thiago.appguiavacinacao.model.CalVacinas;
import com.thiago.appguiavacinacao.model.UniVac;

public class PostoAdapter extends BaseAdapter {

    private Context ctx;
    private UniVac[] postos;

    public PostoAdapter (Context ctx, UniVac[] postos) {
        this.ctx = ctx;
        this.postos = postos;

    }


    @Override
    public int getCount() {
        return (postos != null) ? postos.length : 0;
    }

    @Override
    public Object getItem(int i) {
        return postos[i];
    }

    @Override
    public long getItemId(int i) {
        return Long.parseLong(postos[i].getRpa());
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {


        // Criar a view
        ViewHolder holder = null;

        if (convertView == null) {
            convertView = LayoutInflater.from(ctx).inflate(R.layout.item_posto, null);

            holder = new ViewHolder();
            holder.imgLogo = (ImageView) convertView.findViewById(R.id.imgLogo);
            holder.txtPosto = (TextView) convertView.findViewById(R.id.txtPosto);
            holder.txtEndereco = (TextView) convertView.findViewById(R.id.txtEndereco);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.imgLogo.setImageResource(R.mipmap.ic_launcher);
        holder.txtPosto.setText(postos[i].getUnidade());
        holder.txtEndereco.setText(postos[i].getEndereco());

        return convertView;
    }

    static class ViewHolder {
        ImageView imgLogo;
        TextView txtPosto;
        TextView txtEndereco;
    }
}
