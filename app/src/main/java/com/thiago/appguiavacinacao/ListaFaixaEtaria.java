package com.thiago.appguiavacinacao;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.google.gson.Gson;
import com.thiago.appguiavacinacao.adapter.VacinaAdapter;
import com.thiago.appguiavacinacao.model.CalVacinas;

public class ListaFaixaEtaria extends AppCompatActivity {

    private ListView mListaVacinas;
    private Gson mGson;
    private CalVacinas[] mVacinas;
    private VacinaAdapter mVacinaAdapter;
    private ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_faixa_etaria);

        Intent intent = new Intent();
        intent.putExtra("key2","Eu vim do Lista Faixa Etaria");
        setResult(RESULT_OK,intent);

    }
}
