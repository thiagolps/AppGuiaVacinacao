package com.thiago.appguiavacinacao;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.NotificationCompat;

/**
 * Created by thiago on 28/04/17.
 */

public class Util {
    //URLs do JSon declaradas como constantes
    public static final String sURL1 = "http://200.238.105.91/CalendarioVacinas.json";
    public static final String sURL2 = "http://200.238.105.91/UnidadeSaude.json";


    private ConnetionChanged mListener;

    private static Util instance;
    private NotificationManager mNotificationManager;
    private static final int NOTIFICATION_ID = 1000;
    private Context mContext;

    public static Util getInstance(Context c){
        if (instance == null) {
            instance = new Util(c);
        }
        return instance;
    }

    private Util(Context context){

        mContext = context;
        mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    public void registerListener(ConnetionChanged listener){
        mListener = listener;
    }

    public boolean isOnline() {

        ConnectivityManager cm = (ConnectivityManager)
                mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        //should check null because in airplane mode it will be null
        return (netInfo != null && netInfo.isConnected());
    }

    public void notifyListener(){
        if (mListener != null) mListener.onConnectionChanged();
    }

    public interface ConnetionChanged{
        public void onConnectionChanged();
    }

    public void createNotification(){

        NotificationCompat.Builder mBuilder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(mContext)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("Status da conexão")
                        .setContentText((isOnline()) ? "Online" : "Offline");

        Intent resultIntent = new Intent(mContext, MainActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(mContext);

        stackBuilder.addParentStack(MainActivity.class);

        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);

        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    public void cancelNotification(){
        mNotificationManager.cancel(NOTIFICATION_ID);
    }




}
