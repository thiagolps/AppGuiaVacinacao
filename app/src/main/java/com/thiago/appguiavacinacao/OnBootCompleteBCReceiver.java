package com.thiago.appguiavacinacao;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


/**
 * Created by rbertoli on 05/05/17.
 */

public class OnBootCompleteBCReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
    Intent it = new Intent (context,MainActivity.class);
        it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(it);

    }
}
