package com.thiago.appguiavacinacao;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.support.design.widget.Snackbar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.thiago.appguiavacinacao.adapter.VacinaAdapter;
import com.thiago.appguiavacinacao.model.CalVacinas;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ListaVacinas extends AppCompatActivity {

    private ListView mListaVacinas;
    private Gson mGson;
    private CalVacinas[] mVacinas;
    private VacinaAdapter mVacinaAdapter;
    private ProgressDialog mProgress;

    private final int REQUEST_CODE=1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_vacinas);

        Button btnVoltar = (Button) findViewById(R.id.btnVoltar);
        btnVoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mListaVacinas = (ListView) findViewById(R.id.listaVacinas);
        mListaVacinas.setEmptyView(findViewById(android.R.id.empty));

          //Carrega os dados e mostra diálogo de progresso
          new ListaVacinas.AsyncDataTask().execute();

        //Botão para chamar a ActivityForResult da Lista de Faixa Etaria de vacinação
        Button btnListaFaixaEtaria = (Button) findViewById(R.id.btnListaFaixaEtaria);
        btnListaFaixaEtaria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(ListaVacinas.this, ListaFaixaEtaria.class);
                startActivityForResult(it,REQUEST_CODE);

            }
        });


    }

    private void loadData() {
                try {
                    mGson = new Gson();
                    CalVacinas[] vacinas;
                    //recebe a URL do JSon
                    URL url = new URL(Util.sURL1);
                    //Configura os parametros da conexao http e conecta
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setReadTimeout(10 * 1000);
                    connection.setConnectTimeout(15 * 1000);
                    connection.setRequestMethod("GET");
                    connection.setDoInput(true);
                    connection.setDoOutput(false);
                    connection.connect();

                    if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {

                        final BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                        vacinas = mGson.fromJson(reader, CalVacinas[].class);
                        mVacinas = vacinas;
                        //Caso retorne HTTP OK (200) recebe os dados do json
                    } else {
                        // Retorna mensagem de erro de conexão
                        Snackbar.make(mListaVacinas, getString(R.string.erro_conexao), Snackbar.LENGTH_LONG).show();
                    }

                } catch (IOException e) {
                    // Retorna mensagem de erro de conexão
                    Snackbar.make(mListaVacinas, getString(R.string.erro_conexao), Snackbar.LENGTH_LONG).show();

                }

    }


    private class AsyncDataTask extends AsyncTask<Object, Object, Object> {

        @Override
        protected void onPreExecute() {
            //Mostra o progresso enquanto baixa dados
            mProgress = new ProgressDialog(ListaVacinas.this);
            mProgress.setTitle(R.string.titulo_espera);
            mProgress.setMessage(getString(R.string.mensagem_espera));
            mProgress.show();

        }

        @Override
        protected Object doInBackground(Object... params) {
            //Carrega os dados em segundo plano
            loadData();
            return null;
        }


        @Override
        protected void onPostExecute(Object result) {
            //Dispensa a caixa de dialogo de progresso depois de baixar os dados
            if (mProgress != null && mProgress.isShowing()) {
                try {
                    mProgress.dismiss();
                } catch (Exception e) {

                }
            }
            //Configura a listview
            setupListView();

        }

        }
    private void setupListView() {
        //Seta os dados no adapter
        mVacinaAdapter = new VacinaAdapter(ListaVacinas.this,mVacinas);
        mListaVacinas.setAdapter(mVacinaAdapter);
        Rodape();

        mListaVacinas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View v, int i, long l) {
                //Ao clicar no item, recebe os detalhes daquela posição do ListView
                CalVacinas VacDetalhe  = (CalVacinas) adapterView.getItemAtPosition(i);
                String vac = VacDetalhe.getVacina();
                String doe = VacDetalhe.getDoenca();
                String ida = VacDetalhe.getIdade();
                String dos = VacDetalhe.getDose();
                //Seta os parametros a ser enviados a proxima tela
                Bundle extras = new Bundle();
                extras.putString("vacina",vac);
                extras.putString("doenca",doe);
                extras.putString("idade",ida);
                extras.putString("dose",dos);
                //Chama a tela de detalhes
                Intent it = new Intent(ListaVacinas.this, DetalheVacina.class);
                it.putExtras(extras);

                startActivity(it);
            }
        });
    }

    private void Rodape() {
        final int PADDING = 10;
        final TextView txtFooter = new TextView(this);
        txtFooter.setText(getResources().getQuantityString(
                R.plurals.itens_rodape,
                mVacinaAdapter.getCount(),
                mVacinaAdapter.getCount()));
        txtFooter.setBackgroundColor(Color.LTGRAY);
        txtFooter.setGravity(Gravity.LEFT);
        txtFooter.setPadding(0, PADDING, PADDING, PADDING);
        mListaVacinas.addFooterView(txtFooter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==REQUEST_CODE){
            if(resultCode==RESULT_OK) {
                String result = data.getStringExtra("key2");
                Toast.makeText(this,result,Toast.LENGTH_LONG).show();
            }
        }
    }
}
