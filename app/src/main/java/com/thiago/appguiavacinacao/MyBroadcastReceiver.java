package com.thiago.appguiavacinacao;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class MyBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Util.getInstance(context).createNotification();
        Util.getInstance(context).notifyListener();
    }
}
