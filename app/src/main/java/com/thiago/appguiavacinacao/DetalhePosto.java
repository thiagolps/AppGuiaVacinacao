package com.thiago.appguiavacinacao;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DetalhePosto extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe_posto);

        Intent it = getIntent();
        if (it != null)

        {
            Bundle extras = it.getExtras();
            final String unidade = extras.getString("unidade");
            String endereco = extras.getString("endereco");
            String bairro = extras.getString("bairro");
            String fone = extras.getString("fone");
            final String latitude = extras.getString("latitude");
            final String longitude = extras.getString("longitude");

            TextView Unidade = (TextView) findViewById(R.id.txtDetUnidade);
            Unidade.setText(unidade);
            TextView Endereco = (TextView) findViewById(R.id.txtDetEndereco);
            Endereco.setText(endereco);
            TextView Bairro = (TextView) findViewById(R.id.txtDetBairro);
            Bairro.setText(bairro);
            TextView Fone = (TextView) findViewById(R.id.txtDetFone);
            Fone.setText("(81) " + fone);
            Button btnNavega = (Button) findViewById(R.id.btnNavi);
            btnNavega.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Uri mapUri = Uri.parse("geo:0,0?q="+latitude+","+longitude+" (" + unidade + ")");
                    Intent mIntent = new Intent(Intent.ACTION_VIEW, mapUri);
                    mIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mIntent);
                }
            });
        }




        Button btnVoltar = (Button) findViewById(R.id.btnVoltar);
        btnVoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
