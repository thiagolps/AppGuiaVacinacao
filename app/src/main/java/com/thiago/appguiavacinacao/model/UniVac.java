package com.thiago.appguiavacinacao.model;

/**
 * Created by rbertoli on 02/05/17.
 */

import java.io.Serializable;
import com.google.gson.annotations.SerializedName;

public class UniVac implements Serializable{

    @SerializedName("rpa")
    private String Rpa;
    @SerializedName("unidade")
    private String Unidade;
    @SerializedName("endereco")
    private String Endereco;
    @SerializedName("bairro")
    private String Bairro;
    @SerializedName("fone")
    private String Fone;
    @SerializedName("latitude")
    private String Latitude;
    @SerializedName("longitude")
    private String Longitude;

    public String getRpa ()
    {
        return Rpa;
    }

    public void setRpa (String Rpa)
    {
        this.Rpa = Rpa;
    }

    public String getUnidade ()
    {
        return Unidade;
    }

    public void setUnidade (String Unidade)
    {
        this.Unidade = Unidade;
    }

    public String getEndereco ()
    {
        return Endereco;
    }

    public void setEndereco (String Endereco)
    {
        this.Endereco = Endereco;
    }

    public String getBairro ()
    {
        return Bairro;
    }

    public void setBairro (String Bairro)
    {
        this.Bairro = Bairro;
    }

    public String getFone ()
    {
        return Fone;
    }

    public void setFone (String Fone)
    {
        this.Fone = Fone;
    }
    public String getLatitude ()
    {
        return Latitude;
    }

    public void setLatitude (String Latitude)
    {
        this.Latitude = Latitude;
    }
    public String getLongitude ()
    {
        return Longitude;
    }

    public void setLongitude (String Longitude)
    {
        this.Longitude = Longitude;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [rpa = "+Rpa+", unidade = "+Unidade+", endereco = "+Endereco+", bairro = "+Bairro+", fone = "+Fone+", latitude = "+Latitude+", longitude = "+Longitude+"]";
    }
}
