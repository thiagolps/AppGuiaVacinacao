package com.thiago.appguiavacinacao.model;

/**
 * Created by rbertoli on 02/05/17.
 */

import java.io.Serializable;
import com.google.gson.annotations.SerializedName;


public class CalVacinas implements Serializable{

    @SerializedName("IDADE")
    private String Idade;
    @SerializedName("VACINA")
    private String Vacina;
    @SerializedName("DOENCA_QUE_PROTEGE")
    private String Doenca;
    @SerializedName("DOSE")
    private String Dose;
    @SerializedName("DOSEV")
    private String Dosevol;
    @SerializedName("VIA_DE_ADMINISTRACAO")
    private String Via;
    @SerializedName("ID")
    private String Id;

    public String getIdade ()
    {
        return Idade;
    }

    public void setIdade (String Idade)
    {
        this.Idade = Idade;
    }

    public String getVacina ()
    {
        return Vacina;
    }

    public void setVacina (String Vacina)
    {
        this.Vacina = Vacina;
    }

    public String getDoenca ()
    {
        return Doenca;
    }

    public void setDoenca (String Doenca)
    {
        this.Doenca = Doenca;
    }

    public String getDose ()
    {
        return Dose;
    }

    public void setDose (String Dose)
    {
        this.Dose = Dose;
    }

    public String getDosevol ()
    {
        return Dosevol;
    }

    public void setDosevol (String Dosevol)
    {
        this.Dosevol = Dosevol;
    }

    public String getVia ()
    {
        return Via;
    }

    public void setVia (String Via)
    {
        this.Via = Via;
    }

    public String getID ()
    {
        return Id;
    }

    public void setid (String ID)
    {
        this.Id = Id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [IDADE = "+Idade+" , VACINA = "+Vacina+" , DOENCA_QUE_PROTEGE = "+Doenca+" , DOSE = "+Dose+" , DOSEV = "+Dosevol+" , VIA_DE_ADMINISTRACAO = "+Via+" , ID = "+Id+"]";

    }
}

