package com.thiago.appguiavacinacao;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.thiago.appguiavacinacao.Util.ConnetionChanged;

public class MainActivity extends AppCompatActivity implements ConnetionChanged{

    private ConnectionReceiver mConnectionReveiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Botão para chamar a Activity da Lista de vacinas
        Button btnVacinas = (Button) findViewById(R.id.btnVacinas);
        btnVacinas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(MainActivity.this, ListaVacinas.class);
                startActivity(it);

            }
        });

        //Botão para chamar a Activity da Lista de Postos
        Button btnPostos = (Button) findViewById(R.id.btnPostos);
        btnPostos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(MainActivity.this, ListaPostos.class);
                startActivity(it);

            }
        });


        //Exibe na tela o status da conexao
        setConnectionStatus();

        Util.getInstance(this).registerListener(this);

        Util.getInstance(this).cancelNotification();

    }
    //Verifica se há conexão disponivel
    public void setConnectionStatus(){
        TextView tvwConnectionStatus = (TextView)findViewById(R.id.tvwConnectionStatus);
        tvwConnectionStatus.setText((Util.getInstance(this).isOnline()) ? "Online" : "Offline");
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Em caso de retorno da activity, registra novamente no broadcast receiver
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        mConnectionReveiver = new ConnectionReceiver();
        registerReceiver(mConnectionReveiver, intentFilter);
        setConnectionStatus();

    }

    @Override
    protected void onPause() {
        super.onPause();
        //Em caso de pausa da activity, desregistra do broadcast receiver
        unregisterReceiver(mConnectionReveiver);
    }

    @Override
    public void onConnectionChanged() {
        setConnectionStatus();
    }

    public class ConnectionReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            setConnectionStatus();
        }
    }
}
