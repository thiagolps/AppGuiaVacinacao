package com.thiago.appguiavacinacao;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.support.design.widget.Snackbar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.thiago.appguiavacinacao.adapter.PostoAdapter;
import com.thiago.appguiavacinacao.model.UniVac;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ListaPostos extends AppCompatActivity {

    private ListView mListaPostos;
    private Gson mGson;
    private UniVac[] mPostos;
    private PostoAdapter mPostoAdapter;
    private ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_postos);

        Button btnVoltar = (Button) findViewById(R.id.btnVoltar);
        btnVoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mListaPostos = (ListView) findViewById(R.id.listaPostos);
        mListaPostos.setEmptyView(findViewById(android.R.id.empty));

        //Carrega os dados e mostra diálogo de progresso
        new ListaPostos.AsyncDataTask().execute();


    }

    private void loadData() {
        try {
            mGson = new Gson();
            UniVac[] postos;

            //Recebe a URL do JSon
            URL url = new URL(Util.sURL2);

            //Seta os parametros da conexao http e faz a conexão
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(10 * 1000);
            connection.setConnectTimeout(15 * 1000);
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.setDoOutput(false);
            connection.connect();

            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {

                final BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                postos = mGson.fromJson(reader, UniVac[].class);
                mPostos = postos;
            //Caso retorne HTTP OK (200) recebe os dados do json
            } else {
                // Retorna mensagem de erro de conexão
                Snackbar.make(mListaPostos, getString(R.string.erro_conexao), Snackbar.LENGTH_LONG).show();
            }

        } catch (IOException e) {
            // Retorna mensagem de erro de conexão
            Snackbar.make(mListaPostos, getString(R.string.erro_conexao), Snackbar.LENGTH_LONG).show();

        }

    }


    private class AsyncDataTask extends AsyncTask<Object, Object, Object> {

        @Override
        protected void onPreExecute() {
            //Mostra o progresso enquanto baixa dados
            mProgress = new ProgressDialog(ListaPostos.this);
            mProgress.setTitle(R.string.titulo_espera);
            mProgress.setMessage(getString(R.string.mensagem_espera));
            mProgress.show();

        }

        @Override
        protected Object doInBackground(Object... params) {
            //Carrega os dados em segundo plano
            loadData();
            return null;

        }


        @Override
        protected void onPostExecute(Object result) {
            //Dispensa a caixa de dialogo de progresso depois de baixar os dados
            if (mProgress != null && mProgress.isShowing()) {
                try {
                    mProgress.dismiss();
                } catch (Exception e) {

                }
            }
            //Carrega o listview
            setupListView();
        }

    }

    private void setupListView() {
        //Seta os dados no adapter
        mPostoAdapter = new PostoAdapter(ListaPostos.this,mPostos);
        mListaPostos.setAdapter(mPostoAdapter);

        Rodape();

        mListaPostos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View v, int i,long l) {
                //Ao clicar no item, recebe os detalhes daquela posição do ListView
                   UniVac UniDetalhe  = (UniVac) adapterView.getItemAtPosition(i);

                String uni = UniDetalhe.getUnidade();
                String end = UniDetalhe.getEndereco();
                String bai = UniDetalhe.getBairro();
                String fon = UniDetalhe.getFone();
                String lat = UniDetalhe.getLatitude();
                String lon = UniDetalhe.getLongitude();

                //Converte , para . nas coordenadas
                lat = lat.replaceAll(",",".");
                lon = lon.replaceAll(",",".");

                //Seta os parametros a ser enviados a proxima tela
                Bundle extras = new Bundle();
                extras.putString("unidade",uni);
                extras.putString("endereco",end);
                extras.putString("bairro",bai);
                extras.putString("fone",fon);
                extras.putString("latitude",lat);
                extras.putString("longitude",lon);
                //Chama a tela de detalhes
                Intent it = new Intent(ListaPostos.this, DetalhePosto.class);
                 it.putExtras(extras);

                startActivity(it);
            }
        });
    }

    private void Rodape() {
        final int PADDING = 10;
        final TextView txtFooter = new TextView(this);
        txtFooter.setText(getResources().getQuantityString(
                R.plurals.itens_rodape,
                mPostoAdapter.getCount(),
                mPostoAdapter.getCount()));
        txtFooter.setBackgroundColor(Color.LTGRAY);
        txtFooter.setGravity(Gravity.LEFT);
        txtFooter.setPadding(0, PADDING, PADDING, PADDING);
        mListaPostos.addFooterView(txtFooter);
    }
}
